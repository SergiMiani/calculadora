
package refactoring;

public class Cercle {
    
    public int radi;

    public Cercle(int radi) {
        this.radi = radi;

    }

    public double area() {
        return 3.14 * radi * radi;
    }

    public double perimetre() {
        return 2 * 3.14 * radi;
    }

    public static void main(String[] args) {
        Cercle[] cercles = new Cercle[4];
        for (int i = 0; i < 4; i++) {
            cercles[i] = new Cercle(4 - i);
        }

        for (Cercle c : cercles) {
            System.out.printf("[Area: %.2f, Perimetre: %.2f]", c.area(), c.perimetre());
            System.out.print("\n");
        }
    }
}
